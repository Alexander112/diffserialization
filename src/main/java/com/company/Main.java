package com.company;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;
import java.util.Random;

import com.carrotsearch.sizeof.RamUsageEstimator;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.file.SeekableByteArrayInput;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.simpleframework.xml.*;
import org.simpleframework.xml.core.Persister;

import javax.xml.bind.JAXBException;

public class Main {

    public static String[] marks, models, colors, capitals;
    public static int[] maxSpeeds = {150,200,220,250,270,300,320,350,370};

    public static void main(String[] args) throws Exception {
        // write your code here

        marks  = Files.lines(Paths.get("src/main/resources/marks"), StandardCharsets.UTF_8).findFirst().get().split(",");
        models  = Files.lines(Paths.get("src/main/resources/models"), StandardCharsets.UTF_8).findFirst().get().split(",");
        colors  = Files.lines(Paths.get("src/main/resources/colors"), StandardCharsets.UTF_8).findFirst().get().split(",");
        capitals = Files.lines(Paths.get("src/main/resources/capitals"), StandardCharsets.UTF_8).findFirst().get().split(",");

        int n = 50;
        Garage[] garages = new Garage[n];

        FileWriter standartWriter = new FileWriter("src/main/resources/result/standart.csv");
        FileWriter simpleWriter = new FileWriter("src/main/resources/result/simple.csv");
        FileWriter protoWriter = new FileWriter("src/main/resources/result/proto.csv");
        FileWriter jaxbWriter = new FileWriter("src/main/resources/result/jaxb.csv");
        FileWriter avroWriter = new FileWriter("src/main/resources/result/avro.csv");

        standartWriter.append("Size;Size;Ser;DeSer;Total" + '\n');
        simpleWriter.append("Size;Size;Ser;DeSer;Total" + '\n');
        protoWriter.append("Size;Size;Ser;DeSer;Total" + '\n');
        jaxbWriter.append("Size;Size;Ser;DeSer;Total" + '\n');
        avroWriter.append("Size;Size;Ser;DeSer;Total" + '\n');

        System.out.println("Всего " + n + " гаражей");

        for (int i = 0; i < n; i++) {
            garages[i] = generateGarage(10000 * i);
            System.out.println("Заполнено " + i + " гаражей");
            writeResult(standartWriter,standartSerialization(garages[i]));
            writeResult(simpleWriter,simpleFMSerialization(garages[i]));
            writeResult(protoWriter,protoBufferSerialization(garages[i]));
            writeResult(jaxbWriter,JAXBSerialization(garages[i]));
            writeResult(avroWriter,ApacheAvroSerialization(garages[i]));


        }
        standartWriter.flush();
        simpleWriter.flush();
        protoWriter.flush();
        jaxbWriter.flush();
        avroWriter.flush();

        standartWriter.close();
        simpleWriter.close();
        protoWriter.close();
        jaxbWriter.close();
        avroWriter.close();

        System.out.println(1);

    }

    public static Data standartSerialization(Garage myGarage) throws IOException, ClassNotFoundException {
        long startTime,middleTime,endTime;

        startTime = System.nanoTime();

        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(myGarage);
        oos.flush();
        oos.close();
        fos.flush();
        fos.close();

        middleTime = System.nanoTime();

        ByteArrayInputStream fis = new ByteArrayInputStream(fos.toByteArray());
        ObjectInputStream oin = new ObjectInputStream(fis);
        Garage anotherGarage = (Garage) oin.readObject();
        oin.close();
        fis.close();

        endTime = System.nanoTime();

        Data data = new Data(
                "Standart Serialization",
                (middleTime - startTime)/1000000.0,
                (endTime - middleTime)/1000000.0,
                (endTime - startTime)/1000000.0,
                RamUsageEstimator.humanSizeOf(myGarage),
                RamUsageEstimator.sizeOf(myGarage)
        );

        return data;

    }

    public static Data simpleFMSerialization(Garage myGarage) throws Exception {
        long startTime,middleTime,endTime;

        startTime = System.nanoTime();

        Serializer serializer = new Persister();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        serializer.write(myGarage, os);
        os.flush();
        os.close();

        middleTime = System.nanoTime();

        ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
        Garage anotherGarage = serializer.read(Garage.class, is);
        is.close();

        endTime = System.nanoTime();
        Data data = new Data(
                "Simple Framework Serialization",
                (middleTime - startTime)/1000000.0,
                (endTime - middleTime)/1000000.0,
                (endTime - startTime)/1000000.0,
                RamUsageEstimator.humanSizeOf(myGarage),
                RamUsageEstimator.sizeOf(myGarage)
        );

        return data;

    }

    public static Data protoBufferSerialization(Garage myGarage) throws InvalidProtocolBufferException {
        long startTime,middleTime,endTime;

        startTime = System.nanoTime();

        byte[] garage = myGarage.toProtoByteArray();

        middleTime = System.nanoTime();

        Garage anotherGarage = new Garage(garage);

        endTime = System.nanoTime();

        Data data = new Data(
                "Standart Serialization",
                (middleTime - startTime)/1000000.0,
                (endTime - middleTime)/1000000.0,
                (endTime - startTime)/1000000.0,
                RamUsageEstimator.humanSizeOf(myGarage),
                RamUsageEstimator.sizeOf(myGarage)
        );

        return data;

    }

    public static Data JAXBSerialization(Garage myGarage) throws JAXBException, IOException{
        long startTime,middleTime,endTime;

        startTime = System.nanoTime();

        Parser parser = new JaxbParser();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        parser.saveObject(os,myGarage);
        os.flush();
        os.close();

        middleTime = System.nanoTime();

        ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
        Garage anotherGarage = (Garage)parser.getObject(is,Garage.class);
        is.close();

        endTime = System.nanoTime();

        Data data = new Data(
                "Standart Serialization",
                (middleTime - startTime)/1000000.0,
                (endTime - middleTime)/1000000.0,
                (endTime - startTime)/1000000.0,
                RamUsageEstimator.humanSizeOf(myGarage),
                RamUsageEstimator.sizeOf(myGarage)
        );

        return data;

    }

    public static Data ApacheAvroSerialization(Garage myGarage) throws IOException {
        long startTime,middleTime,endTime;
        Schema garageSchema = new Schema.Parser().parse(new File("src/main/avro/garage.asvc"));


        startTime = System.nanoTime();

        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<>(garageSchema);
        DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<>(datumWriter);
        dataFileWriter.create(garageSchema, fos);
        dataFileWriter.append(myGarage.toAvroClass());
        dataFileWriter.close();
        fos.flush();
        fos.close();

        middleTime = System.nanoTime();

        SeekableByteArrayInput fis = new SeekableByteArrayInput(fos.toByteArray());
        DatumReader<GenericRecord> datumReader = new GenericDatumReader<>(garageSchema);
        DataFileReader<GenericRecord> dataFileReader = new DataFileReader<>(fis, datumReader);
        GenericRecord avroGarage = null;
        while (dataFileReader.hasNext()) {
            avroGarage = dataFileReader.next(myGarage.toAvroClass());
        }
        fis.close();
        Garage anotherGarage = new Garage(avroGarage);

        endTime = System.nanoTime();

        Data data = new Data(
                "Standart Serialization",
                (middleTime - startTime)/1000000.0,
                (endTime - middleTime)/1000000.0,
                (endTime - startTime)/1000000.0,
                RamUsageEstimator.humanSizeOf(myGarage),
                RamUsageEstimator.sizeOf(myGarage)
        );

        return data;

    }

    public static int getRandom (int min,int max) {
        Random rn = new Random();
        return rn.nextInt(max - min + 1) + min;
    }

    public static ArrayList<Car> generateCarsList (int n) {
        ArrayList<Car> cars = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            cars.add(new Car(
                    marks[getRandom(0,marks.length-1)],
                    maxSpeeds[getRandom(0,maxSpeeds.length-1)],
                    models[getRandom(0,models.length-1)],
                    colors[getRandom(0,colors.length-1)],
                    0));
        }
        return cars;
    }

    public static Garage generateGarage(int carsNum) {
        Garage garage = new Garage(
                colors[getRandom(0,colors.length-1)],
                getRandom(200,1500),
                getRandom(200,1500),
                getRandom(200,1500),
                capitals[getRandom(0,capitals.length-1)]
        );
        garage.setCars(generateCarsList(carsNum));

        return garage;
    }

    public static String formatResult(double result) {
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.FRANCE);

        return formatter.format("%.6f",result).toString();

    }

    public static void writeResult (FileWriter writer, Data result) throws IOException {

        writer.append(result.getHumanSize() + ';');
        writer.append(String.valueOf(result.getSize()) + ';');
        writer.append(formatResult(result.getSerTime()) + ';');
        writer.append(formatResult(result.getDeSerTime()) + ';');
        writer.append(formatResult(result.getTotalTime()) + '\n');

    }
}
