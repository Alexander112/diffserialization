package com.company;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public interface Parser {
    Object getObject(InputStream is, Class c) throws JAXBException;
    void saveObject(OutputStream os, Object o) throws JAXBException;
}