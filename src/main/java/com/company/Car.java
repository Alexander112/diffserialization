package com.company;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.lang.instrument.Instrumentation;

@XmlRootElement(name = "car")
@XmlType(propOrder = {"name","model","color","maxSpeed","milage"})
@Root
public class Car implements Serializable {
    @Element
    private String name, model, color;

    @Element
    private int maxSpeed, milage;

    public Car(
            @Element(name="name") String name,
            @Element(name="maxSpeed") int maxSpeed,
            @Element(name="model") String model,
            @Element(name="color") String color,
            @Element(name="milage") int milage)
    {

        this.name = name;
        this.maxSpeed = maxSpeed;
        this.model = model;
        this.color = color;
        this.milage = milage;

    }

    public Car(){}

    public String getColor() {
        return color;
    }
    @XmlElement
    public void setColor(String color) {
        this.color = color;
    }
    @XmlElement
    public void setName(String name) {
        this.name = name;
    }
    @XmlElement
    public void setModel(String model) {
        this.model = model;
    }
    @XmlElement
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
    @XmlElement
    public void setMilage(int milage) {
        this.milage = milage;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getMilage() {
        return milage;
    }

    public void drive(int km){
        this.milage += km;
    }

}