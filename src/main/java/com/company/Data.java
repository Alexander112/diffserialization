package com.company;


public class Data {

    private String serMethod, humanSize;
    private double serTime, deSerTime, totalTime;
    private long size;

    public Data(String serMethod, double serTime, double deSerTime, double totalTime, String humanSize, long size) {
        this.serMethod = serMethod;
        this.serTime = serTime;
        this.deSerTime = deSerTime;
        this.totalTime = totalTime;
        this.humanSize = humanSize;
        this.size = size;
    }

    public String getSerMethod() {
        return serMethod;
    }

    public double getSerTime() {
        return serTime;
    }

    public double getDeSerTime() {
        return deSerTime;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public String getHumanSize() {
        return humanSize;
    }

    public long getSize() {
        return size;
    }
}
