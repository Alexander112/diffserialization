package com.company;

import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import javax.xml.bind.annotation.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

@XmlRootElement(name = "garage")
@XmlType(propOrder = {"width","height","length","carCount","name","color","cars"})
@Root
public class Garage implements Serializable {
    private static int count = 0;

    @Attribute
    private int width, height, length, carCount;

    @Attribute
    private String name,color;

    @ElementList(inline = true,required=false)
    private ArrayList<Car> cars;

    public int getWidth() {
        return width;
    }

    @XmlAttribute
    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    @XmlAttribute
    public void setHeight(int height) {
        this.height = height;
    }

    public int getLength() {
        return length;
    }

    @XmlAttribute
    public void setLength(int length) {
        this.length = length;
    }

    public int getCarCount() {
        return carCount;
    }

    @XmlAttribute
    public void setCarCount(int carCount) {
        this.carCount = carCount;
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    @XmlAttribute
    public void setColor(String color) {
        this.color = color;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    @XmlElement(name = "car")
    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
        this.carCount += cars.size();
    }

    public Garage(){}

    public Garage(
            @Attribute(name="color") String color,
            @Attribute(name="width") int width,
            @Attribute(name="height") int height,
            @Attribute(name="length") int length,
            @Attribute(name="name") String name)
    {
        this.color = color;
        this.width = width;
        this.height = height;
        this.length = length;
        this.carCount = 0;
        this.name = name;
        this.cars = new ArrayList<>();
        Garage.count++;
    }

    public Garage(byte[] ProtoByteArray) throws InvalidProtocolBufferException {

        Protos.ProtoGarage protoGarage = Protos.ProtoGarage.parseFrom(ProtoByteArray);

        this.color = protoGarage.getColor();
        this.width = protoGarage.getWidth();
        this.height = protoGarage.getHeight();
        this.length = protoGarage.getLength();
        this.carCount = protoGarage.getCarCount();
        this.name = protoGarage.getName();
        this.cars = new ArrayList<>();
        Garage.count++;

        for (int i = 0; i < protoGarage.getCarsCount(); i++) {

            Protos.ProtoCar curCar = protoGarage.getCars(i);

            cars.add(new Car(curCar.getName(),curCar.getMaxSpeed(),curCar.getModel(),curCar.getColor(),curCar.getMilage()));
        }
    }

    public Garage(GenericRecord avroGarage) {
        this.width = (int)avroGarage.get("width");
        this.height = (int)avroGarage.get("height");
        this.length = (int)avroGarage.get("length");
        this.name = avroGarage.get("name").toString();
        this.color = avroGarage.get("color").toString();
        this.carCount = (int)avroGarage.get("carCount");
        this.cars = new ArrayList<>();
        Garage.count++;

        GenericData.Array cars = (GenericData.Array)avroGarage.get("cars");

        for (int i = 0; i <cars.size(); i++) {

            GenericData.Record curCar = (GenericData.Record)cars.get(i);
            this.cars.add(new Car(
                    curCar.get("name").toString(),
                    (int)curCar.get("maxSpeed"),
                    curCar.get("model").toString(),
                    curCar.get("color").toString(),
                    (int)curCar.get("milage")
                )
            );
        }


    }

    public void addCar(Car car) {
        this.cars.add(car);
    }

    public byte[] toProtoByteArray() {

        Protos.ProtoGarage.Builder builder = Protos.ProtoGarage.newBuilder()
                .setCount(count)
                .setWidth(width)
                .setHeight(height)
                .setLength(length)
                .setName(name)
                .setColor(color);

        for (int i = 0; i < cars.size(); i++) {
            Car curCar = cars.get(i);
            builder.addCars(Protos.ProtoCar.newBuilder()
                    .setColor(curCar.getColor())
                    .setModel(curCar.getModel())
                    .setMaxSpeed(curCar.getMaxSpeed())
                    .setName(curCar.getName())
                    .setMilage(curCar.getMilage()));
        }

        Protos.ProtoGarage protoGarage = builder.build();

        return protoGarage.toByteArray();

    }

    public GenericRecord toAvroClass() throws IOException {

        Schema garageSchema = new Schema.Parser().parse(new File("src/main/avro/garage.asvc"));
        Schema carSchema = new Schema.Parser().parse(new File("src/main/avro/car.asvc"));

        GenericRecord avroGarage = new GenericData.Record(garageSchema);

        ArrayList<GenericRecord> cars = new ArrayList<>();

        for (int i = 0; i < this.cars.size(); i++) {

            GenericRecord avroCar = new GenericData.Record(carSchema);
            Car curCar = this.cars.get(i);

            avroCar.put("name",curCar.getName());
            avroCar.put("model",curCar.getModel());
            avroCar.put("color",curCar.getColor());
            avroCar.put("maxSpeed",curCar.getMaxSpeed());
            avroCar.put("milage",curCar.getMilage());

            cars.add(avroCar);

        }

        avroGarage.put("width", width);
        avroGarage.put("height", height);
        avroGarage.put("length", length);
        avroGarage.put("carCount", carCount);
        avroGarage.put("name", name);
        avroGarage.put("color",color);
        avroGarage.put("cars",cars);

        return avroGarage;


    }

}
